# ROM source patches

color="\033[0;32m"
end="\033[0m"

echo -e "${color}Applying patches${end}"
sleep 1


# Clone kernel
git clone https://github.com/Notganesh/kernel_xiaomi_chime-pyro.git -b pyro kernel/xiaomi/chime

# Clone vendor
git clone https://github.com/Notganesh/vendor_xiaomi_chime.git -b 13.0 vendor/xiaomi/chime
git clone https://gitlab.com/chimeoss/vendor_xiaomi_citrus -b lineage-20 vendor/xiaomi/citrus
git clone https://gitlab.com/chimeoss/vendor_xiaomi_lime -b lineage-20 vendor/xiaomi/lime

# Hardware
git clone https://github.com/Notganesh/hardware_xiaomi.git -b lineage-20 hardware/xiaomi
